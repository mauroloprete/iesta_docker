import sqlalchemy
from db import db_engine

def create_schema(engine):
    
    ddl_str = """
    create schema if not exists scraping_precios;
    """
    
    print("Creando Schema!")
    
    ddl_sql = sqlalchemy.text(ddl_str)
    
    engine.execute(ddl_sql)

def create_dimProductos(engine):
    
    ddl_str = """
    create table if not exists scraping_precios.d_productos
        (
            producto       varchar,
            marca          varchar,
            especificacion varchar,
            nombre         varchar,
            id_producto    integer
            constraint id_producto
                primary key
        ); 
    """
    
    print("Creando dim_productos!")

    ddl_sql = sqlalchemy.text(ddl_str)
    
    engine.execute(ddl_sql)
    
def create_dimEstablecimientos(engine):
    
    ddl_str = """
    
    create table if not exists scraping_precios.d_establecimientos
        (
          razon_social    varchar,
          nombre_sucursal varchar,
          direccion       varchar,
          ccz             integer,
          barrio          varchar,
          cajas           integer,
          cadena          varchar,
          long            varchar,
          lat             varchar,
          ciudad          varchar,
          depto            varchar,
          id_depto         varchar,
          localidad       varchar,
          id_establecimiento integer 
            constraint id_establecimiento
                primary key
        );
    """
    
    print("Creando dim_establecimientos")
    
    ddl_sql = sqlalchemy.text(ddl_str)
    
    engine.execute(ddl_sql)
    

def create_factPrice(engine):
    
    ddl_str = """
    
    
    create table if not exists scraping_precios.fact_price
    (
        id_establecimientos integer,
        id_producto        integer,
        id_file integer
        constraint id_file
            references scraping_precios.a_procesar (id),
        precio              double precision,
        fecha               timestamp,
        oferta              integer
    );
    
    
    """
    
    ddl_sql = sqlalchemy.text(ddl_str)
    
    engine.execute(ddl_sql)
    
def create_aProcesar(engine):
    
    ddl_str = """
    
    create table if not exists scraping_precios.a_procesar 
    (   
        id serial PRIMARY KEY,
        filename varchar,
        file_path varchar,
        md5 varchar,
        year integer,
        procesado integer,
        fecha_listado date        
    );
    """
    
    
    ddl_sql = sqlalchemy.text(ddl_str)
    
    print("Creando tabla a_procesar!")
    
    engine.execute(ddl_sql)


def init_db():
    
    engine = db_engine()
    
    create_schema(engine)
    
    create_aProcesar(engine)
    
    create_dimEstablecimientos(engine)
    
    create_dimProductos(engine)
    
    create_factPrice(engine)
        
    print("Base de datos inicializada!")
    