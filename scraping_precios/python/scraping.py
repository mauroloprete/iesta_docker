import urllib.request
from bs4 import BeautifulSoup
import wget
import os
import hashlib


def scraper(url):
    
    hrefs = get_links(url)
    
    download_files(hrefs)
    


def get_links(url):
    opener = urllib.request.FancyURLopener({})
    f = opener.open(url)
    content = f.read()

    soup = BeautifulSoup(
        content,
        features = "html.parser"
    )

    hrefs = soup.find_all(
        'a',
        {
            "class": "Button Button--primary Button--small u-mb1 u-mr2 no-link-hover"
        },
        href = True
    )
    
    return hrefs

def download_files(hrefs):

    for i in hrefs:
        filenames = os.listdir(
            './data-raw'
        )
        
        if os.path.basename(i['href']) not in filenames:
            
            message = 'Descargando ' + os.path.basename(i['href'])

            print(message)
            
            wget.download(
                i['href'],
                out = "./data-raw"
            )
            
            


