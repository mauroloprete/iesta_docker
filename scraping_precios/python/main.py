from scraping import scraper
from db import open_connection, close_connection, db_engine
from schemas import init_db
from etls import etl


# Indicar el URL de datos abiertos.    

url = "https://catalogodatos.gub.uy/dataset/declaraciones-al-sistema-de-informacion-de-precios-al-consumidor-2019"

scraper(url)

init_db()

engine = db_engine()

etl(
    dir = "./data-raw", 
    engine = engine
)
