import hashlib
import os
import pandas as pd
from datetime import datetime
import re
import sqlalchemy

def list_files_todb(dir,engine):
    
    df_dir = list_files(dir)
    
    df_db = pd.read_sql(
        """
        SELECT
            filename,
            md5
        FROM scraping_precios.a_procesar
        """,
        engine
    )
    
    df_todb = df_dir.merge(
        df_db,
        on = ['filename','md5'],
        how = "outer",
        indicator=True
    )
    
    df_todb = df_todb[~(df_todb._merge == 'both')].drop('_merge', axis=1)
    
    df_todb['procesado'] = 0
    df_todb['fecha_listado'] = datetime.now()
    
    files_todb(df_todb,engine)
    

def files_todb(df,engine):
    
    df.to_sql(
        'a_procesar',
        schema = "scraping_precios",
        con = engine,
        if_exists = 'append',
        index = False
    )
    
    
def list_files(dir):
    

    csv_files = os.listdir(
        dir
    )

    df = []

    for file in csv_files:
        
        if file.endswith('.csv'):
            
            file_path = os.getcwd() + '/data-raw/' + file
            
            with open(file_path,'rb') as file_to_check:
                
                file_data = file_to_check.read()
                md5 = hashlib.md5(file_data).hexdigest()
                year = re.findall(r'\d+',file)
                
                try:
                    df.append(
                        [
                            file,
                            file_path,
                            md5,
                            int(year[0])
                        ]
                    )
                except:
                    df.append(
                        [
                            file,
                            file_path,
                            md5,
                            0
                        ]
                    )
                
                


    return pd.DataFrame(
        df,
        columns= [
            'filename',
            'file_path',
            'md5',
            'year'
        ]
    )


def load_dimensions(
    filepath, 
    engine
):  
    
    map_dimensions = {
        "/usr/src/app/data-raw/establecimiento.csv": "d_establecimientos",
        "/usr/src/app/data-raw/productos.csv": "d_productos"
    }
    
    l_dimensions = list(
        map_dimensions.keys()
    )
    
    if filepath in l_dimensions:
       
        df = pd.read_csv(
            filepath,
            sep = ";",
            encoding='latin-1',
            header = 0
        )
        
        df = df.rename(columns=lambda x: re.sub('\.','_',x))
        
        try:
            df.to_sql(
                map_dimensions[filepath],
                engine,
                schema="scraping_precios",
                if_exists="append",
                index=False
            )
        except:
            print("Tabla de dimensión ya cargada!")
        
        
        


def set_procesado(md5,engine):
    
    str = "UPDATE scraping_precios.a_procesar set procesado = 1 WHERE md5 = '{md5}'".format(
        md5 = md5
    )
    
    print(str)
    
    
    sql_str = sqlalchemy.text(str)
    
    engine.execute(sql_str)


def load_fact(id,filepath,engine):
    
    
    d_str = "DELETE FROM scraping_precios.fact_price WHERE id_file = {id}".format(id = id)
    
    sql_str = sqlalchemy.text(d_str)
    
    print(sql_str)
        
    engine.execute(sql_str)
            
    year = int(re.findall(r'\d+', filepath)[0])
    
    if year < 2018:
        
        chunk_size = 50000
        
        batch_no = 1
        
        for chunk in pd.read_csv(
            filepath,
            sep = ",",
            encoding = "latin-1",
            names=['id_establecimientos', 'id_producto',
                   'precio', 'fecha', 'oferta'],
            chunksize = chunk_size
        ):
            
            chunk['id_file'] = id
            
            chunk.to_sql(
                "fact_price",
                schema = "scraping_precios",
                if_exists='append',
                index=False,
                con = engine
            )
            
            batch_no += 1
            
        
    else:
        
        chunk_size = 50000
        
        batch_no = 1
        
        for chunk in pd.read_csv(
            filepath,
            sep = ",",
            encoding="latin-1",
            names = [
                'id_precio_diario',
                'declaracion',
                'fecha',
                'fecha_anterior',
                'oferta',
                'precio',
                'precioanterior',
                'publico',
                'id_establecimientos',
                'id_ferias',
                'id_producto'
            ]
        ):
            
            chunk['id_file'] = id
            
            tmp = chunk[[
                'id_establecimientos',
                'id_producto',
                'precio',
                'fecha',
                'oferta',
                'id_file'
            ]]
            
            tmp.to_sql(
                "fact_price",
                schema = "scraping_precios",
                if_exists='append',
                index=False,
                con = engine
            )
            
            batch_no += 1

            print(batch_no)
            
            
        
    
    
    
def staging_toProduction(engine):

    staging = pd.read_sql(
        """
        SELECT
            id,
            file_path,
            md5,
            year
        FROM scraping_precios.a_procesar
        WHERE procesado = 0
        """,
        engine
    )
    
    staging['file_path'].apply(lambda x: load_dimensions(x,engine))
    
    staging.query('year != 0').apply(lambda row: load_fact(row['id'],row['file_path'],engine=engine),axis=1)
        
    staging['md5'].apply(lambda x:set_procesado(x,engine))
        
    
    
    
    
def etl(dir,engine):
    
    list_files_todb(
        dir,
        engine
    )
    
    print("Los datos estan en staging!")
    
    
    staging_toProduction(engine)
    
    print("Los datos están en producción!")


