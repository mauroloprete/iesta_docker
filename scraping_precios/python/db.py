import psycopg2
from sqlalchemy import create_engine
import os

def db_engine(
    host = os.environ["POSTGRES_HOST"],
    user=os.environ["POSTGRES_USER"],
    password = os.environ["POSTGRES_PASSWORD"],
    database=os.environ["POSTGRES_DATABASE"],
    port = os.environ["POSTGRES_PORT"]
):
    
    url = 'postgresql+psycopg2://{username}:{password}@{host}:{port}/{database}'.format(
        username = user,
        password = password,
        host = host,
        port = port,
        database = database
    )
    
    return create_engine(
        url, 
        pool_recycle=3600
    )

def open_connection():
    
    db_engine().connect()
    
def close_connection(cnn):
    cnn.close();
    